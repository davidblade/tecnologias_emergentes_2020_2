//require('./bootstrap');
import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
window.axios = require("axios");
window.req = axios.create({
    baseUrl:'/'
})


new Vue({
    router,
    render:h=>h(App)
}).$mount('#app')