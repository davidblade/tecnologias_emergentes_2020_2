import VueRouter from 'vue-router' // importando el plugin para utilizar rutas en vue
import SingIn from '../pages/signin'    // importando el template para inicio de sesion
import SingUp from '../pages/signup' 
import Home from '../pages/home'
import Vue from "vue"

Vue.use(VueRouter)// vue utilizara lo que es las rutas del plugin

const routes = [
    {name:'login', path: '/user/signin', component: SingIn },
    {name:'register',path: '/user/signup', component: SingUp },
    {name:'home',path: '/dashboard/home', component: Home }
]

const router = new VueRouter({
    routes
})

export default router