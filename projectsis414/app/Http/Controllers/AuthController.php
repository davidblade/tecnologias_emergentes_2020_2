<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\log;

class AuthController extends Controller
{
    public function init(){
        $user = Auth::user(); //usuario que este logeado
        return ["user"=>$user];
    }

    public function logout(){
        Auth::logout();
        return ["logout"=>true];
    }


    public function signin(Request $request){//iniciar sesion
        if(Auth::attempt(["username"=> $request->username,"password"=>$request->password])){
            $user = Auth::user();
            return ["user" => $user];
        }else{
            return response()->json(["error"=>"Usuario no existe"],403);
        }
    }

    public function signup(Request $request){//registrar usuario
        $data = $request->only('name','username','email','password');
        error_log(json_encode($data));
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email; 
        $user->password = bcrypt($request->password);//encriptamos la contrasenia
        $user->save();
        //Auth::login($user);
        return ["user" => $user];
    }
}